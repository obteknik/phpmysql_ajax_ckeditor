<?php require_once('init.inc.php'); 
// $r = $mysqli->query("SELECT c.*, e.* FROM contenu c, emplacement e WHERE c.idEmplacement = e.idEmplacement")->fetch_all(MYSQLI_ASSOC);
//$r = $mysqli->query("SELECT c.*, e.* FROM contenu c, emplacement e WHERE c.idEmplacement = e.idEmplacement");
$r = $pdo->query("SELECT c.*, e.* FROM contenu c, emplacement e WHERE c.idEmplacement = e.idEmplacement");
//$r->fetch();

$page = array();
while($d = $r->fetch())
{
	$page[$d['nomSysteme']][$d['idContenu']] = $d;
}
// print '<pre>'; print_r($page); print '</pre>';
$r = $pdo->query("SELECT * FROM rubrique");
?>
<!Doctype html>
<html>
	<head>
		<script src="ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="ckeditor/ckfinder/ckfinder.js"></script>
		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<link rel="stylesheet" href="style.css" />
		<meta charset="utf-8" />
	</head>
	<body>
		<span id="message"></span>
		<header>
			<div class="conteneur">
				<div contenteditable="true" class="logo contentEditable" data-idContenu="<?php echo $page['accueil'][1]['idContenu']; ?>" data-idEmplacement="<?php echo $page['accueil'][1]['idEmplacement']; ?>" title="cliquez sur moi pour me modifier"><?php echo $page['accueil'][1]['contenu']; ?></div>
				<div contenteditable="true" class="slogan contentEditable" data-idContenu="<?php echo $page['accueil'][2]['idContenu']; ?>" data-idEmplacement="<?php echo $page['accueil'][2]['idEmplacement']; ?>" title="cliquez sur moi pour me modifier"><?php echo $page['accueil'][2]['contenu']; ?></div>
				<div class="clear"></div>
			</div>
		</header>
		<nav>
			<div class="conteneur">
				<?php
				while($d = $r->fetch())
				{
					echo "<a href=\"pageEditable.php\" contenteditable=\"true\" class=\"contentEditable\" data-idRubrique=\"$d[idRubrique]\" title=\"cliquez sur moi pour me modifier\">$d[titre]</a>";
				}
				?>
			</div>
		</nav>
		<section>
			<div class="conteneur">
				<div class="gauche">
					<h1 contenteditable="true" class="contentEditable" data-idContenu="<?php echo $page['accueil'][3]['idContenu']; ?>" data-idEmplacement="<?php echo $page['accueil'][3]['idEmplacement']; ?>" title="cliquez sur moi pour me modifier"><?php echo $page['accueil'][3]['contenu']; ?></h1>
					<div contenteditable="true" class="contentEditable" data-idContenu="<?php echo $page['accueil'][4]['idContenu']; ?>" data-idEmplacement="<?php echo $page['accueil'][4]['idEmplacement']; ?>" title="cliquez sur moi pour me modifier">
					  <?php echo $page['accueil'][4]['contenu']; ?>
					</div>
				
				</div>
				<div class="droite">
					<div contenteditable="true" class="contentEditable" data-idContenu="<?php echo $page['accueil'][5]['idContenu']; ?>" data-idEmplacement="<?php echo $page['accueil'][5]['idEmplacement']; ?>" title="cliquez sur moi pour me modifier">
						<?php echo $page['accueil'][5]['contenu']; ?>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</section>
		<footer>
			<div class="conteneur">
				<div contenteditable="true" class="contentEditable" data-idContenu="<?php echo $page['accueil'][6]['idContenu']; ?>" data-idEmplacement="<?php echo $page['accueil'][6]['idEmplacement']; ?>" title="cliquez sur moi pour me modifier">
					<?php echo $page['accueil'][6]['contenu']; ?>
				</div>
			</div>
		</footer>
		<hr />
		<br /><br />
		
		<script type="text/javascript">
		$(function()
		{
			$('.contentEditable').on('click', function()
			{
			//	alert("clic content editable");
				$('.liveEdit').removeClass( "liveEdit" ); // je retire toutes les classes liveEdit qu'il pourrait y avoir suite à de précédents changements (je ne souhaite travailler que sur une seule zone).
				$('#mybutt').remove(); // évite d'avoir plusieurs boutons si plusieurs double click.
		//		$('*[contenteditable="true"]').attr('contenteditable', false); // je met tout les autres contenteditable sur false.
		//		$(this).attr('contenteditable', true); // ce contenteditable passe à true (celui sur lequel on a cliqué).		
				$(this).after('<button id="mybutt">Valider les changements</button>');
				$(this).addClass('liveEdit');
				eventBtn(this); // permet d'exécuter la fonction contenant le traitement du button.
			});
			function eventBtn(parent)
			{
				$('#mybutt').click(function(e)
				{ // si l'on clic sur le bouton
					// alert("clic mybutt");
					e.preventDefault(); // on annule le comportement initial par défaut
					// var parent = $(this).parent('.contentEditable');
					// alert(parent);
					$(this).remove(); // on supprime le bouton
					var contenu = $(parent).html(); // on récupère le contenu de notre div.
					var idContenu = $(parent).attr('data-idContenu');
					var idEmplacement = $(parent).attr('data-idEmplacement');
					if(!idContenu || !idEmplacement) var idRubrique = $(parent).attr('data-idRubrique');
					console.log("contenu = " + contenu);
					console.log("idContenu = " + idContenu);
					console.log("idEmplacement = " + idEmplacement);
					console.log("idRubrique = " + idRubrique);
			//		$('*[contenteditable="true"]').attr('contenteditable', false);	
					if(!idContenu || !idEmplacement){ var chaine = 'menu=1&contenu=' +contenu+'&idRubrique=' +idRubrique } // passage d'arguments pour le menu
					else{ var chaine = 'texte=1&contenu=' +contenu+'&idContenu=' +idContenu+'&idEmplacement=' +idEmplacement } // passage d'arguments pour le contenu
					$.ajax({
						type: 'post',
						url:  'sql.php',
						data: chaine
					});
					$("#message").css('padding', '5px'); // on ajoute de la marge intérieur
					$("#message").append('Enregistrement ok!').fadeOut(3000); // notre message apparait seulement 3s secondes
					$('.liveEdit').removeClass( "liveEdit" ); // je retire toutes les classes liveEdit qu'il pourrait y avoir suite à de précédents changements (je ne souhaite travailler que sur une seule zone).
				});
			}
		});
		</script>
	</body>
</html>