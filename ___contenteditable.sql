-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 22 Avril 2016 à 18:48
-- Version du serveur :  5.7.9
-- Version de PHP :  5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `contenteditable`
--

-- --------------------------------------------------------

--
-- Structure de la table `contenu`
--

DROP TABLE IF EXISTS `contenu`;
CREATE TABLE IF NOT EXISTS `contenu` (
  `idContenu` int(3) NOT NULL AUTO_INCREMENT,
  `idEmplacement` int(3) NOT NULL,
  `contenu` text NOT NULL,
  PRIMARY KEY (`idContenu`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `contenu`
--

INSERT INTO `contenu` (`idContenu`, `idEmplacement`, `contenu`) VALUES
(1, 1, '<p><img src="/ckfinder/userfiles/images/gregory.gif" data-cke-saved-src="/ckfinder/userfiles/images/gregory.gif" style="width: 64px; height: 64px;" alt=""><br></p>'),
(2, 1, '<p><strong>Titre de mon site web !</strong><br></p>'),
(3, 1, '<span style="color:#FF0000">Accueil</span><br>'),
(4, 1, '<p>Premier paragrapahe !!!<br></p><p>Je peux Ã©crire autant de texte que je le souhaite !! !!!</p><p>par exemple j''ai oubliÃ© de valider cela...</p>'),
(5, 1, '<p>coucou<br></p>'),
(6, 1, '<p>Texte du footer<br></p>');

-- --------------------------------------------------------

--
-- Structure de la table `emplacement`
--

DROP TABLE IF EXISTS `emplacement`;
CREATE TABLE IF NOT EXISTS `emplacement` (
  `idEmplacement` int(3) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `nomSysteme` varchar(50) NOT NULL,
  PRIMARY KEY (`idEmplacement`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `emplacement`
--

INSERT INTO `emplacement` (`idEmplacement`, `nom`, `nomSysteme`) VALUES
(1, 'Accueil', 'accueil'),
(2, 'Qui Sommes Nous ?', 'qui-sommes-nous'),
(3, 'Nos Projets', 'nos-projets'),
(4, 'Notre Activité', 'notre-activites'),
(5, 'Contact', 'contact');

-- --------------------------------------------------------

--
-- Structure de la table `rubrique`
--

DROP TABLE IF EXISTS `rubrique`;
CREATE TABLE IF NOT EXISTS `rubrique` (
  `idRubrique` int(3) NOT NULL AUTO_INCREMENT,
  `titre` varchar(50) NOT NULL,
  `lien` varchar(50) NOT NULL,
  PRIMARY KEY (`idRubrique`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `rubrique`
--

INSERT INTO `rubrique` (`idRubrique`, `titre`, `lien`) VALUES
(1, 'Accueil', ''),
(2, 'Qui Sommes Nous ?', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
